from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class Flipkart():
    """Class of all data about flipkart scrapping"""
    def __init__(self, search):
        self.name = "flipkart"
        self.url = "https://www.flipkart.com/"
        #class name of lists
        self.linkId1 = "_31qSD5"
        self.linkId2 = "_3dqZjq"
        #class name of input
        self.inputId = "LM6RPg"
        self.next = "_3fVaIS"
        self.searchString = search
        self.nextLimit = 50

    def getLinksFlipkart(self):
        driver = webdriver.Firefox()
        driver.get(self.url)

        assert "Online" in driver.title
        searchInput = driver.find_element_by_class_name(self.inputId)
        searchInput.clear()
        searchInput.send_keys(self.searchString)
        searchInput.send_keys(Keys.RETURN)
        linkId = self.linkId1
        try:
            WebDriverWait(driver, 12).until(
            EC.presence_of_element_located((By.CLASS_NAME, linkId))
            )
        except:
            linkId = self.linkId2
            try:
                WebDriverWait(driver, 12).until(
                EC.presence_of_element_located((By.CLASS_NAME, linkId))
                )
            except:
                #they might have changed class name
                return 412

        returnList = list()
        passed = False

        for i in range(self.nextLimit):
            try:
                WebDriverWait(driver, 12).until(
                EC.presence_of_element_located((By.CLASS_NAME, linkId))
                )
            except:
                #they might have changed class name
                break
            
            #driver.implicitly_wait(2)
            links = driver.find_elements_by_class_name(linkId)
            for x in links:
                returnList.append(x.get_attribute("href"))
                try:
                    WebDriverWait(driver, 1).until(
                        EC.presence_of_element_located((By.CLASS_NAME, self.next))
                    )
                except:
                    break;
            nextLink = driver.find_elements_by_class_name(self.next)
            if(passed):
                nextLinkurl = nextLink[1].get_attribute("href")
            else:
                nextLinkurl = nextLink[0].get_attribute("href")
                passed = True
            driver.get(nextLinkurl)

        self.takeDown(driver)
        return returnList

    def takeDown(self, driver):
        driver.close()


if __name__ == "__main__":
    obj = Flipkart("iphone 8s")
    print (obj.getLinksFlipkart())
